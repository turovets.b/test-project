<?php

namespace App\Service;

use App\Dto\ExchangeRateDto;
use App\Entity\Currency;
use App\Repository\CurrencyRepository;

class CurrencyService
{
    private $exchangeService;

    private $repository;

    public function __construct(ExchangeService $exchangeService, CurrencyRepository $repository)
    {
        $this->exchangeService = $exchangeService;
        $this->repository = $repository;
    }

    public function getCurrency(ExchangeRateDto $filters) 
    {
        $existedCurrency = $this->repository->findOneBy([
            'date' => $filters->date,
            'base' => $filters->from,
            'rate' => $filters->to
        ]);
        
        if($existedCurrency) {
            return $existedCurrency->toArray();
        }

        $rates = $this->exchangeService->getRates($filters);
        
        $currency = new Currency();

        $currency->setBase($filters->from)
            ->setRate($filters->to)
            ->setDate($filters->date)
            ->setValue($rates->getRate());
        
        return $this->repository->save($currency)->toArray();
    }

    public function getFromInterval(ExchangeRateDto $filters)
    {
        $result = [
            'from' => $filters->from,
            'to' => $filters->to
        ];
        $rates = $this->exchangeService->getRatesByInterval($filters);  
        
        foreach($rates as $key => $value) {
            
            $temp = [
                'date' => $key,
                'rate' => $value[$filters->from]
            ];

            $result['rate'][] = $temp;
            
        }

        return $result;
    }
}