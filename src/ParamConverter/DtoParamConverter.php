<?php

namespace App\ParamConverter;

use App\Dto\DtoInterface;
use App\Exception\ApiValidationFailedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DtoParamConverter implements ParamConverterInterface
{
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function apply(Request $request, ParamConverter $configuration)
    {
        $class = $configuration->getClass();
        $options = $configuration->getOptions();
        $validatorGroups = $options['validator']['groups'] ?? null;

        $data = $request->request->all() + $request->files->all() + $request->query->all();
        $dto = new $class($data);

        $errors = $this->validator->validate($dto, null, $validatorGroups);
        
        if (count($errors) > 0) {
            throw new ApiValidationFailedException(400, $errors, 'validation failed');
        }

        $request->attributes->set($configuration->getName(), $dto);

        return true;
    }

    public function supports(ParamConverter $configuration)
    {
        $class = $configuration->getClass();

        if (!$class) {
            return false;
        }

        $reflection = new \ReflectionClass($class);

        return $reflection->implementsInterface(DtoInterface::class);
    }
}