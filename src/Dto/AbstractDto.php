<?php

namespace App\Dto;

use App\Traits\FromArrayTrait;

abstract class AbstractDto implements DtoInterface
{
    use FromArrayTrait;

    public function __construct(array $data = [])
    {
        $this->fromArray($data);
    }
}