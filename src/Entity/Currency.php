<?php

namespace App\Entity;

use App\Repository\CurrencyRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass=CurrencyRepository::class)
 * @ORM\Table(name="currency")
 * @JMS\ExclusionPolicy("all")
 */
class Currency extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\Expose()
     */
    private $base;

    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\Expose()
     */
    private $rate;

    /**
     * @ORM\Column(type="date")
     * @JMS\Expose()
     */
    private $date;

    /**
     * @ORM\Column(type="float")
     * @JMS\Expose()
     */
    private $value;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBase(): ?string
    {
        return $this->base;
    }

    public function setBase(string $base): self
    {
        $this->base = $base;

        return $this;
    }

    public function getRate(): string
    {
        return (string)$this->rate;
    }

    public function setRate(string $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getValue(): float
    {
        return (float)$this->value;
    }

    public function setValue(float $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'from' => $this->base,
            'to' => $this->rate,
            'date' => $this->date->format('Y-m-d'),
            'rate' => $this->value
        ];
    }
}
