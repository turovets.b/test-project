<?php

namespace App\Dto;

interface DtoInterface
{
    public function __construct(array $data);

    public function fromArray(array $data);
}