<?php

namespace App\Service;

use App\Dto\ExchangeRateDto;
use BenMajor\ExchangeRatesAPI\ExchangeRatesAPI;
use BenMajor\ExchangeRatesAPI\Exception;
use BenMajor\ExchangeRatesAPI\Response;

class ExchangeService
{
    private $client;

    public function __construct()
    {
        $this->client = new ExchangeRatesAPI();
    }

    /**
     * Get specific rates
     *
     * @param ExchangeRateDto $exchangeRateDto
     * @return array
     */
    public function getRates(ExchangeRateDto $exchangeRateDto)
    {
        $rates = $this->client
            ->addRate($exchangeRateDto->from)
            ->setBaseCurrency($exchangeRateDto->to)
            ->addDateTo($exchangeRateDto->date->format("Y-m-d"))
            ->fetch();
   
        return $rates;
    }

    public function getRatesByInterval(ExchangeRateDto $filters)
    {
        $rates = $this->client
            ->addRate($filters->from)
            ->setBaseCurrency($filters->to)
            ->addDateFrom('2021-03-19')
            ->addDateTo($filters->date->format("Y-m-d"))
            ->fetch()
            ->getRates();
        
        return $rates;
    }
}