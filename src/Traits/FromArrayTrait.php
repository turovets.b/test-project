<?php

namespace App\Traits;

trait FromArrayTrait
{
    public function fromArray(array $data)
    {
        foreach ($data as $property => $value) {
            if (!$value) {
                continue;
            }

            if (property_exists($this, $property)) {
                $reflectedProperty = new \ReflectionProperty($this, $property);

                if ($reflectedProperty->isPublic()) {
                    $this->$property = $value;
                }
            }

            $method = 'set' . ucfirst($property);

            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }
}