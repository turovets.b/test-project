<?php

namespace App\Repository;

use App\Dto\ExchangeRateDto;
use App\Entity\Currency;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CurrencyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Currency::class);
    }

    public function save($entity, bool $flush = true)
    {
        $this->_em->persist($entity);

        if ($flush) {
            $this->_em->flush();
        }

        return $entity;
    }
}
