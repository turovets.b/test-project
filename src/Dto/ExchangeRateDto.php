<?php

namespace App\Dto;

use DateTime;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Date;

class ExchangeRateDto extends AbstractDto
{
    /**
     * @var string
     * @Assert\Regex("/[A-Z]{3}/")
     * @Assert\NotBlank()
     */
    public $from;

    /**
     * @var string
     * @Assert\Regex("/[A-Z]{3}/")
     * @Assert\NotBlank()
     */
    public $to;

    /**
     * @var \DateTime
     */
    public $date;

    public function __construct(array $data = [])
    {
        $this->date = new \DateTime("now");
        parent::__construct($data);
    }

    public function getDate(): ?\DateTime
    {
        return $this->registrationDate;
    }

    public function setDate(string $date): self
    {
        $this->date = new \DateTime($date);
        return $this;
    }


}