<?php

namespace App\Controller;

use App\Entity\Currency;
use App\Dto\ExchangeRateDto;
use App\Service\CurrencyService;
use App\Service\ExchangeService;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Contracts\Translation\TranslatorInterface;

class CurrencyController extends BaseController
{
    private $service;

    public function __construct(CurrencyService $service)
    {
        $this->service = $service;
    }

    /**
     * @OA\Tag(name="exchange-rate")
     * @OA\Get(
     *     path="/api/exchange-rate",
     *     summary="Get the exchange rate for the specified date."
     * )
     * @OA\Parameter(name="from",
     *     in="query",
     *     description="Базовый код валюты, с которого производится расчет курса обмена",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *         example="RUB"
     *     )
     * ),
     * @OA\Parameter(name="to",
     *     in="query",
     *     description="Код валюты, на который производится расчет курса обмена",
     *     required=true,
     *     @OA\Schema(
     *         type="string",
     *         example="USD"
     *     )
     * ),
     * @OA\Parameter(name="date",
     *     in="query",
     *     description="Дата (если дата не указана, то используется сегодняшняя дата).",
     *     required=false,
     *     @OA\Schema(
     *         type="date",
     *         example="2021-02-10",
     *     )
     * )
     * @OA\Response(
     *     response="400",
     *     description="validation error",
     *     @OA\JsonContent(ref="#/components/schemas/ValidationError")
     * ),
     * @OA\Response(
     *     response="200",
     *     description="success",
     *     @Model(type=Currency::class)
     * )
     * @Rest\Route("/api/exchange-rate", methods={"GET"})
     */
    public function getCurrencyRate(ExchangeRateDto $filters)
    {
        return $this->service->getCurrency($filters);
    }

    /**
     * @OA\Tag(name="exchange-rate")
     * @OA\Get(
     *     path="/api/exchange-rates",
     *     summary="Get a list of exchange rates for a specified period of dates"
     * )
     * @Rest\Route("/api/exchange-rates", methods={"GET"})
     */
    public function getExchangeFromInterval(ExchangeRateDto $filters)
    {
        return $this->service->getFromInterval($filters);
    }
}
