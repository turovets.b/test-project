<?php

namespace App\EventListener;

use App\Exception\ApiValidationFailedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ExceptionListener
{
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();

        $content = [
            'code' => 0,
            'message' => $exception->getMessage(),
            'errors' => []
        ];

        if ($exception instanceof ApiValidationFailedException) {
            $content['code'] = $exception->getStatusCode();
            $content['errors'] = $exception->getErrors();
        } elseif ($exception instanceof HttpExceptionInterface) {
            $content['code'] = $exception->getStatusCode();
        } else {
            $content['code'] = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        $response = new JsonResponse($content);
        $event->setResponse($response);
    }
}