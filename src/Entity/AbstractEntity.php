<?php

namespace App\Entity;

use App\Traits\FromArrayTrait;


abstract class AbstractEntity
{
    use FromArrayTrait;

    public function __construct(array $properties = [])
    {
        $this->fromArray($properties);
    }
}