<?php

namespace App\Virtual\Model;

use OpenApi\Annotations as OA;
use JMS\Serializer\Annotation as JMS;

class ValidationError
{
    /**
     * @var int
     * @JMS\Type(name="integer")
     */
    public $code;

    /**
     * @var string
     * @JMS\Type(name="string")
     */
    public $message;

    /**
     * @var array
     * @JMS\Type("array<string, array<string>>")
     * @OA\Property(
     *     property="errors",
     *     @OA\Property(
     *         property="from",
     *         @OA\Items(example="wrong type")
     *     )
     * )
     */
    public $errors;
}