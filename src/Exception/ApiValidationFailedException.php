<?php

namespace App\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ApiValidationFailedException extends HttpException
{
    private $violations;

    public function __construct(
        int $statusCode,
        $violations,
        string $message = null,
        \Throwable $previous = null,
        array $headers = [],
        ?int $code = 0
    )
    {
        $this->violations = $violations;
        parent::__construct($statusCode, $message, $previous, $headers, $code);
    }

    public function getErrors()
    {
        $errors = $this->violations;

        if ($this->violations instanceof ConstraintViolationListInterface) {
            $errors = [];

            foreach ($this->violations as $violation) {
                $errors[$violation->getPropertyPath()][] = $violation->getMessage();
            }
        }

        return $errors;
    }
}