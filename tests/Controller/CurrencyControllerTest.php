<?php

namespace App\Tests\Controller;

use App\DataFixtures\CurrencyFixtures;
use App\Tests\BaseWebTestCase;

/**
 * Class CurrencyControllerTest
 * @package App\Tests\Controller
 *
 * @group currency
 */
class CurrencyControllerTest extends BaseWebTestCase
{
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->loadFixtures([
            CurrencyFixtures::class
        ])->getReferenceRepository();
    }

    /**
     * @param array $content
     */
    public function checkCurrencyStructure(array $content): void
    {
        $this->assertArrayHasKey('from', $content);
        $this->assertArrayHasKey('to', $content);
        $this->assertArrayHasKey('date', $content);
        $this->assertArrayHasKey('rate', $content);
    }

    /**
     * @covers \App\Controller\CurrencyController::getCurrencyRate
     */
    public function testGetCurrencyRate()
    {
        $this->client->request('GET', "/api/exchange-rate?from=RUB&to=USD");
        $content = $this->getContent();

        $this->assertStatusCode(200);
        $this->checkCurrencyStructure($content);
    }

    /**
     * @covers \App\Controller\CurrencyController::getCurrencyRate
     */
    public function testGetExistedCurrencyRate()
    {
        $existedCurrency = $this->repository->getReference(CurrencyFixtures::TEST_CURRENCY_RATE);

        $this->client->request('GET', "/api/exchange-rate?from=RUB&to=USD&date=2021-12-31");
        $content = $this->getContent();
        
        $this->assertStatusCode(200);

        $this->checkCurrencyStructure($content);
        $this->assertEquals($existedCurrency->getValue(), $content['rate']);
    }
}