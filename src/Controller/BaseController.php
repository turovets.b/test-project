<?php

namespace App\Controller;

use App\Exception\ApiValidationFailedException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Form\FormInterface;

abstract class BaseController extends AbstractFOSRestController
{
    /**
     * @param FormInterface $form
     * @return array
     */
    protected function getErrorsFromForm(FormInterface $form): array
    {
        $errors = [];

        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }

        return $errors;
    }

    /**
     * @param FormInterface $form
     */
    protected function createValidationErrorResponse(FormInterface $form)
    {
        $errors = $this->getErrorsFromForm($form);
        $message = 'validation.failed';

        throw new ApiValidationFailedException(400, $errors, $message);
    }

    /**
     * Gets form with validation if needed
     *
     * @param string $type
     * @param array $data
     * @param null $entity
     * @param array $options
     * @param bool $exception
     * @return FormInterface
     */
    protected function getForm(string $type, array $data, $entity = null, array $options = [], bool $exception = true): FormInterface
    {
        $form = $this->createForm($type, $entity, $options);
        $form->submit($data, false);

        if ($exception && !$form->isValid()) {
            $this->createValidationErrorResponse($form);
        }

        return $form;
    }
}