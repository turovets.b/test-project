<?php

namespace App\DataFixtures;

use App\Entity\Currency;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CurrencyFixtures extends Fixture
{
    const TEST_CURRENCY_RATE = 'test_currency_rate';

    public function load(ObjectManager $manager)
    {
        $date = new \DateTime();
        $data = [
            'base' => 'RUB',
            'rate' => 'USD',
            'date' => $date->setDate(2021, 12, 31),
            'value' => 777,
            'reference' => self::TEST_CURRENCY_RATE
        ];

        $currency = new Currency($data);

        $manager->persist($currency);
        $this->addReference($data['reference'], $currency);
        $manager->flush();
    }
}
