<?php

namespace App\Tests;

use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

abstract class BaseWebTestCase extends WebTestCase
{
    use FixturesTrait;
    /**
     * @var KernelBrowser
     */
    protected $client;

    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    protected function assertStatusCode(int $code)
    {
        $this->assertSame($code, $this->client->getResponse()->getStatusCode());
    }

    public function getContent(): array
    {
        return json_decode($this->client->getResponse()->getContent(), true);
    }
}